
// DOT NOT CHANGE THIS FILE
const payload = require("./payload.json");

let data = payload;

exports.setData = (newData) => {
  data = newData;
};

exports.getData = () => data;

exports.initData = () => {
  data = require("./payload.json");
};
