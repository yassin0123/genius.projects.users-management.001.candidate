// DONT DELETE THIS CODE ADD ONLY REQUIRED LINES
var express = require("express");
require("express-async-errors");

const http = require("http");
const config = require("./config");

var app = express();
const server = http.createServer(app);

app.use(express.json({ limit: "50mb" }));

app.use("/status", (req, res) => {
  return res.sendStatus(200);
});

if (process.env.NODE_ENV !== "test") {
  server.listen(config.PORT, config.HOST, () => {
    console.log(`Listening to ${config.HOST}:${config.PORT}`);
  });
}

module.exports = {
  app,
  server,
};
